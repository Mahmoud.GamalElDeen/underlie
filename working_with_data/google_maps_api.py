import pandas as pd
import requests 

google_maps_api_base_url = ''
google_maps_api_key = ''
google_maps_api_input_type = ''

df = pd.read_csv('categores.csv',header=0,names=['cat','count'])

temp_output = dict()

for data in df['cat']:
    res = requests.get(google_maps_api_base_url + data + google_maps_api_input_type + google_maps_api_key)
    temp_output[data] = res.json()

output = pd.DataFrame(temp_output,columns=['name','categories'])
output.to_csv('google_api_output.csv',index=False)
