import os
import sys
from flask import Flask, request, abort, jsonify, redirect, request
from flask_cors import CORS

from werkzeug.utils import secure_filename

import pandas as pd
import numpy as np
import datetime
from sqlalchemy import create_engine
import psycopg2

# create and configure the app
app = Flask(__name__)

CORS(app)

database_name = 'transactions'
database_path = "postgresql://postgres:root@{}/{}".format(
    'localhost:5432', database_name)
# engine = create_engine(database_path)
query3 = "select date,description,debit,credit,c.categorie from trans_details as t join categories c \
        on t.categorie = c.id \
        where date between '2020-6-1%' AND '2021-3-30%';"

cat_query = "select DISTINCT c.categorie from categories_key_words \
        as w  join categories c on c.id = w.categorie \
        where 'SALARY' ILIKE CONCAT('%' ,w.name , '%');"
'''
testing end-point to check server is up 
'''


@app.route('/piechart', methods=['GET'])
def home():
    try:
        from_date = request.args.get('from')+'%'
        to_date = request.args.get('to')+'%'
        sql = f"select date,description,debit,credit,balance,c.categorie,user_id from trans_details as t join categories c \
            on t.categorie = c.id \
            where user_id=1 and date between '{from_date}' AND '{to_date}';"
        pg_conn = psycopg2.connect(database_path)
        cur = pg_conn.cursor()
        cur.execute(sql)
        output = cur.fetchall()
        cur.close()
        names = ['date', 'description', 'debit',
                 'credit', 'balance', 'category', 'usre_id']
        df = pd.DataFrame(output, columns=names)
        d = dict()
        for cat in df['category'].value_counts().index:
            d[cat] = str(df.loc[df['category'] == cat]['debit'].sum())
        total_spending = spending(df)
         
        return jsonify({
            'success': True,
            'data': d,
            'totalSpending':total_spending

        }), 200
    except:
        abort(400)

def spending(df):
    sub_df = df.loc[df['debit'] != 0][['date','debit']]
    data   = sub_df.groupby("date").sum()
    json_data = data.to_json()

    import json

    json_data = json.loads(json_data)
    total_spending = dict()
    for d in json_data['debit']:
        timestamp = datetime.datetime.fromtimestamp(int(d[:-3]))
        total_spending[timestamp.strftime('%Y-%m-%d ')] = json_data['debit'][d]
    return total_spending



@app.route('/category', methods=['POST'])
def index():
    try:
        body = request.get_json()
        description = body.get('desc')
        sql = f"select DISTINCT c.categorie from categories_key_words \
            as w  join categories c on c.id = w.categorie \
            where '{description}' ILIKE CONCAT('%' ,w.name , '%');"
        pg_conn = psycopg2.connect(database_path)
        cur = pg_conn.cursor()
        cur.execute(sql)
        output = cur.fetchall()
        cur.close()
        # print(sql)
        if len(output) == 0:
            category = 'OTHER'
        else:
            category = output[0][0]
        return jsonify({
            'success': True,
            'category': category

        }), 200
    except:
        abort(400)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


ALLOWED_EXTENSIONS = {'csv'}
UPLOAD_FOLDER = 'dashboard'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1000 * 1000 # max file size = 16 megabytes

@app.route('/upload', methods=['POST'])
def upload():
    try:
        if 'file' not in request.files:
                return abort(400)
        file = request.files['file']
        if file.filename == '':
            abort(400)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return jsonify({
                "filename": filename,
                "success": True
            }), 200
    except:
        abort(400)

'''
error handlers
'''


@app.errorhandler(404)
def not_found(error):
    return jsonify({
        'success': False,
        "error": 404,
        "message": "resource not found"
    }), 404


@app.errorhandler(400)
def bad_request(error):
    print("Unexpected error:", sys.exc_info()[0])
    return jsonify({
        'success': False,
        "error": 400,
        "message": "Bad Request"
    }), 400


@app.errorhandler(422)
def unprocessable(error):
    return jsonify({
        'success': False,
        "error": 422,
        "message": "Unprocessable Request"
    }), 422


@app.errorhandler(401)
def unauthorized(error):
    return jsonify({
        'success': False,
        "error": 401,
        "message": "Unauthorized Request"
    }), 401
