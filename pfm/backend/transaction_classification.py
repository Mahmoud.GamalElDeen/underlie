
import nltk
import random
import pandas as pd
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import SGDClassifier
from sklearn.naive_bayes import MultinomialNB
import os

#helping functions
##############################################

def _remove_unwanted_words(words):
    '''remove words has numbers or less than 2 char length'''
    new_words = list()
    for word in words:
        if not _has_number(word) and len(word)>1:
            new_words.append(word)
    return new_words

def _has_number(input_string):
    '''check if text has number in'''
    return any(char.isdigit() for char in input_string)

def transaction_features(description): # the main function
    '''extract features from transaction '''
    description = description.replace('\\',' ').replace('BNK',' ')
    words = description.split()
    new_words = _remove_unwanted_words(words)
    features = dict()
    x = 1
    for word in new_words:
      features[f"{x}'main"] = word
      x+=1
    return features

def scikit_transaction_features(description):
    description = description.replace('\\',' ').replace('BNK',' ')
    words = description.split()
    new_words = _remove_unwanted_words(words)
    
    return " ".join(new_words)


# Load Trained Model 
################################################
import pickle

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

nltk_classifier = pickle.load(open(os.path.join(BASE_DIR,'model/nltk_classifier.sav'), 'rb'))
SGD_clf         = pickle.load(open(os.path.join(BASE_DIR,'model/SGD_classifier.sav'), 'rb'))
MN_clf          = pickle.load(open(os.path.join(BASE_DIR,'model/MN_classifier.sav'), 'rb'))


"""### NLTK Classifier API"""

def nltk_classify(transactions:list) -> list:
    '''return labels and confidance score '''
    labels = list()
    for transaction in transactions:
      processed_trans = transaction_features(transaction)
      label_prob = nltk_classifier.prob_classify(processed_trans)
      # label = classifier.classify(processed_trans)
      label = label_prob.max()
      prob = label_prob.prob(label)
      labels.append((label,prob))
    return labels


"""### Scikit-Learn Classifier API"""

def Scikit_learn_SGD_classify(transactions:list)-> list: 
    '''return labels and confidance score '''
    labels = list()
    processed_trans = [scikit_transaction_features(transaction) for transaction in transactions]
    
    SGD_clf_prediction      = SGD_clf.predict(processed_trans)
    SGD_clf_prediction_prob = [p.max() for p in SGD_clf.predict_proba(processed_trans)]
    labels = [(label,probability) for label,probability in zip(SGD_clf_prediction,SGD_clf_prediction_prob)]
    return labels

def Scikit_learn_NP_classify(transactions:list)-> list: 
    '''return labels and confidance score '''
    labels = list()
    processed_trans = [scikit_transaction_features(transaction) for transaction in transactions]
    
    MN_clf_prediction       = MN_clf.predict (processed_trans)
    MN_clf_prediction_prob  = [p.max() for p in MN_clf.predict_proba(processed_trans)]
    labels = [(label,probability) for label,probability in zip(MN_clf_prediction,MN_clf_prediction_prob)]
    return labels

def main():
    test_trans = ['POS PURCHASE FT21131T74F0\BNK ELMENIAWY CAIRO S 07BEG 234065025264390000000658',
                  'POS PURCHASE FT20154K5LZ8\BNKTOTAL BONJOUR 114702 MATROUH 17AEG00057298 5264390000001425']
    print( 'nltk classification --> ', nltk_classify(test_trans))
    print( 'SGD  classification --> ', Scikit_learn_SGD_classify(test_trans))
    print( 'MN   classification --> ', Scikit_learn_NP_classify(test_trans))


if __name__ == '__main__':
    main()

