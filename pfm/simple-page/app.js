
window.onload = () => { 
    console.log('window loaded');
    document.querySelector('#Categories').click();
};

const catBtn = document.querySelector('#Categories');
const url = baseUrl + backendPort + endPoint;
const from_ele = document.querySelector('#from');
const to_ele = document.querySelector('#to');
catBtn.addEventListener("click", () => {
    console.log('from:', from_ele.value)
    console.log('to:', to_ele.value)
    const from = from_ele.value;
    const to   = to_ele.value;
    getData(url, from, to).then(data => {
        console.log(data)
        const datakeys = Object.keys(data['data']);
        const dataValuesStr = Object.values(data['data']);
        dataValues = []
        for (d in dataValuesStr) {
            dataValues.push(parseInt(dataValuesStr[d]));
        }
        pieChart(datakeys,dataValues)
        thisMonthChart(Object.keys(data['totalSpending']),Object.values(data['totalSpending']))
       

    })
})

function thisMonthChart(labels, series) {
    const options = {
        
        series: [{
            name: "Desktops",
            data: series
        }],
        chart: {
            width: '200%',
            height: '400',
            type: 'line',
            zoom: {
                enabled: false
            }
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'straight'
        },

        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 0.5
            },
        },
        xaxis: {
            categories: labels,
        }
    };
    const chart = new ApexCharts(document.querySelector(".this-month-chart"), options);
    chart.render();
}

function pieChart(labels,series){
    const options = {
        series: series,
        labels: labels,
        chart: {
            type: 'donut',
            width: 400
        },
        dataLabels: {
            enabled: false,

        },
        colors: ['#229e22', '#fc4b50', '#fc5b50', '#fc6b50', '#fc7b50', '#fc8b50'],
        fill: {
            colors: ['#229e22', '#fc4b50', '#fc5b50', '#fc6b50', '#fc7b50', '#fc8b50']
        },
        legend: {
            show: false,
        },
        plotOptions: {
            pie: {
                expandOnClick: false,
                donut: {
                    labels: {
                        show: true,
                        name: {
                            show: true,
                        },
                        value: {
                            show: true,
                        },
                        total: {
                            show: true,
                            label: "Total Month Spending",
                            color: 'black'

                        }
                    }
                }
            }
        }
    }
    const chart = new ApexCharts(document.querySelector(".pie-chart"), options);
    chart.render();
}