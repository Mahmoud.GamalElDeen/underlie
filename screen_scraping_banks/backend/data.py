# import pandas as pd
# import numpy as np
# import datetime


# def convert_str_date(d):
#     try:
#         day = int(d[0:2])
#         month_name = d[2:5].lower()
#         datetime_object = datetime.datetime.strptime(month_name, "%b")
#         month = datetime_object.month

#         year = int('20'+d[5:])

#         return datetime.date(year, month, day)
#     except:
#         return ''


# def isNaN(num):
#     return num != num


# def convert_str_to_int(b):
#     try:
#         balance = b.replace('.', ' ').split()
#         balance = balance[0].replace(',', ' ').split()
#         temp = ''
#         for x in balance:
#             temp += x
#         try:
#             balance = int(temp)
#             return balance
#         except:
#             return 0
#     except:
#         return 0


# '''
# read data
# '''
# excel_file_path = './7.xls'
# # read excel file
# df = pd.read_excel(excel_file_path, dtype=str, skiprows=18, engine='xlrd')

# '''
# clean data
# '''
# # get tail data
# df.dropna(axis=0, how='all', inplace=True)
# df.dropna(axis=1, how='all', inplace=True)
# df.reset_index(drop=True, inplace=True)
# tail = df.iloc[-5]
# tail = pd.DataFrame(tail)
# tail.reset_index(drop=True, inplace=True)
# tail.dropna(inplace=True)
# tail.reset_index(drop=True, inplace=True)
# tail = tail.T
# tail.reset_index(drop=True, inplace=True)
# print('tail data: ')
# print(tail)

# # Series
# date = df['Unnamed: 2']
# description = df['Unnamed: 9']
# debit = df['Unnamed: 18']
# credit = df['Unnamed: 23']
# balance = df['Unnamed: 25']

# # new DataFrame
# data = {'date': date, 'description': description,
#         'debit': debit, 'credit': credit, 'balance': balance}
# data = pd.DataFrame(data)

# # drop nan rows
# data.dropna(axis=0, how='all', inplace=True)
# # reset row indexs after drop
# data.reset_index(drop=True, inplace=True)

# # remove last 5 rows (tail)
# data = data.iloc[:-5]

# # fixing description
# rows = data.shape[0]
# for i in range(rows-2, 0, -1):
#     if(isNaN(data['date'][i])):
#         data['description'][i-1] += data['description'][i]
#         data.drop(i, inplace=True)
# data.reset_index(drop=True, inplace=True)

# # replace Nan with empty value
# data.replace(np.nan, '', inplace=True)

# # change string number to int number for credit, debit, and balance
# data['credit'] = data['credit'].apply(convert_str_to_int)
# data['debit'] = data['debit'].apply(convert_str_to_int)
# data['balance'] = data['balance'].apply(convert_str_to_int)

# # change date from string to datetime format
# data['date'] = data['date'].apply(convert_str_date)
# print('******************************')
# print(data)

# '''
# process data
# '''
# SALARY = pd.DataFrame(
#     columns=['date', 'description', 'debit', 'credit', 'balance'])
# ATM_WITHDRAWAL = pd.DataFrame(
#     columns=['date', 'description', 'debit', 'credit', 'balance'])
# ATM_DEPOSIT = pd.DataFrame(
#     columns=['date', 'description', 'debit', 'credit', 'balance'])
# GAS = pd.DataFrame(columns=['date', 'description',
#                             'debit', 'credit', 'balance'])
# OTHER = pd.DataFrame(
#     columns=['date', 'description', 'debit', 'credit', 'balance'])

# rows = data.shape[0]
# gas_key_words = ['WATANIYA', 'TOTAL', 'FUEL']
# for i in range(0, rows):
#     s = data['description'][i]
#     if('SALARY' in s):

#         SALARY = SALARY.append(data.iloc[i], ignore_index=True)
#         continue
#     elif('ATM' in s):
#         if('WITHDRAWAL' in s):
#             ATM_WITHDRAWAL = ATM_WITHDRAWAL.append(
#                 data.iloc[i], ignore_index=True)
#         else:
#             ATM_DEPOSIT = ATM_DEPOSIT.append(data.iloc[i], ignore_index=True)
#         continue
#     elif any(word in s for word in gas_key_words):
#         GAS = GAS.append(data.iloc[i], ignore_index=True)
#         continue
#     else:
#         OTHER = OTHER.append(data.iloc[i], ignore_index=True)

'''
def total(col):
    t=0
    for i in col:
        try:
            if(np.isnan(i)):
                pass
        except:
            t+=convert_str_to_int(i)
    return t
def convert_str_to_int(b):
    try:
        balance = b.replace('.',' ').split()
        balance = balance[0].replace(',',' ').split()
        temp = ''
        for x in balance :
            temp+=x
        try:
            balance = int(temp)
            return balance
        except:
            return 0
    except:
        return 0

if __name__ == '__main__':
    total_income = []
    total_outcome = []
    # path to excel file to read
    file_pathes = []
    for i in range(1,12):
        file_pathes.append(fr"./{i}.xls")
    # read excel file using pands
    for f in file_pathes:
        data = pd.read_excel(f, dtype=str, skiprows=3, engine='xlrd')
    # convert data to nump element
        df = pd.DataFrame(data).to_numpy()

        details_col_index = 9
        outcome_col_index = 18
        income_col_index = 23

        details_col = df[:, details_col_index]
        outcome_col = df[:, outcome_col_index]
        income_col = df[:, income_col_index]
        total_income.append(total(income_col))
        total_outcome.append(total(outcome_col))
    x=0
    t_i=0
    t_o=0
    for i in total_income:
        t_i+=i
        t_o+=total_outcome[x]
        print(f'total income: {i}, total outcome:{total_outcome[x]}')
        x+=1
    print('--------------------------------------------------------')
    print(f'avr income:{t_i/12}', f'avr_outcome: {t_o/12}')
'''
