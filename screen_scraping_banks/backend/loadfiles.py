import os
import subprocess


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
DOWNLOAD_DIR = os.path.join(BASE_DIR, "dataprocess")
data = [os.path.join('dataprocess', f)
        for f in os.listdir(DOWNLOAD_DIR) if f.endswith(".xls")]
print(data)
'''
for file in os.listdir(DOWNLOAD_DIR):
    if file.endswith(".xls"):
        print( file )
'''


def delete_all(data):
    for d in data:
        subprocess.run(['rm', d])
