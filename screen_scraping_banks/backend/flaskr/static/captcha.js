src = localStorage.getItem("src");
captchaEle = document.querySelector("#captcha-img");
captchaEle.setAttribute("src", src);

username = localStorage.getItem("username");
password = localStorage.getItem("password");

subCapBtn = document.querySelector("#BW_button_802163");

subCapBtn.addEventListener("click", async function (event) {
  event.preventDefault();
  sendCap();
});

const server = "http://127.0.0.1";
const port = "5000";

const sendCap = async () => {
  console.log("Sending CAP");
  const captchaText = document.querySelector("#captchaText").value;
  const data = { username: username, password: password, cap: captchaText };
  submit(`${server}:${port}/cap`, data).then((data) => {
    console.log(data);
    if (!data["success"]) {
      console.log("invalidUserNameOrPassword()");
      location.href = "/cib";
    } else {
      let token = data["jwt"];
      localStorage.setItem("token", token);
      location.href = "/final";
    }
  });
};

const getJwt = () => {
  jwt = localStorage.getItem("token");
  AuthoToken = "Bearer" + " " + jwt;
  return AuthoToken;
};

const submit = async (url = "", data = {}) => {
  const res = await fetch(url, {
    method: "POST",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
      Authorization: getJwt(),
    },
    body: JSON.stringify(data),
  });
  try {
    const newData = await res.json();

    return newData;
  } catch (error) {
    console.log(error);
  }
};
