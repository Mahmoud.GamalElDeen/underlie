import time
from pprint import pprint

import tabula
from selenium.webdriver.common.by import By

from .scrap import Scrap


class HDBScrap(Scrap):
    url = "https://ebanking.hdb-egy.com/ebanking/#/"
    profile_setting_url = ""
    bank = "HDB"

    def scrap(self):
        try:
            self.browser.get(self.url)
            self.browser.implicitly_wait(5)

            log_on_el = self.browser.find_element(
                By.XPATH,
                "/html/body/div[2]/div/ui-view/div/div/section[1]/div/div[1]/div[1]/div/div/a",
            )
            self.browser.execute_script("arguments[0].click();", log_on_el)
            time.sleep(1)

            user_name_el = self.browser.find_element(By.ID, "username")
            user_name_el.send_keys(self.username)

            password_el = self.browser.find_element(By.ID, "password")
            password_el.send_keys(self.password)

            submit_btn = self.browser.find_element(
                By.ID,
                "BW_button_802163",
            )
            submit_btn.click()
            time.sleep(5)
        except Exception as e:
            raise e
        self.browser.close()
        self.browser.quit()
