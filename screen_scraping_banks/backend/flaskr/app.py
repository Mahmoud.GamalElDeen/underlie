import os
import random
from pprint import pprint

from flask import Flask, abort, jsonify, redirect, render_template, request
from flask_cors import CORS
from flask_migrate import Migrate

from .AIB import AIBScrap
from .ALEX import ALEXScrap
from .AUDI import AUDIScrap
from .CIB import CIBScrap
from .cryptography_API import cryptography_API
from .FAB import FABScrap
from .HDB import HDBScrap
from .HSBC import HSBCScrap
from .jwt_api import JWT_API
from .MASHRAQ import MASHRAQScrap
from .MISR import MISRScrap
from .models import BankDetail, BankTransaction, User, db
from .NBE import NBEScrap
from .twilio_api import phoneValidation
from .wrapers import check_auth, get_token_auth_header, require_auth

# create and configure the app
app = Flask(__name__)

database_path = "postgresql://postgres:root@localhost:5432/postgres"
app.config["SQLALCHEMY_DATABASE_URI"] = database_path
db.init_app(app)
migrate = Migrate(app, db)
CORS(app)


# define the health check route
@app.route("/health")
def health_check():
    return "", 204


f = cryptography_API()  # for encryption
cl = JWT_API()  # for creating JWT Authintcation
pv = phoneValidation()


"""
control Allowed methods and headers
"""


@app.after_request
def after_request(response):
    response.headers.add("Access-Control-Allow-Headers", "Content-Type, Authorization")
    response.headers.add(
        "Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE, OPTIONS"
    )
    return response


@app.route("/", methods=["GET"])
def home():
    return render_template("home.html")


@app.route("/cib", methods=["GET", "POST"])
def cib():
    if request.method == "GET":
        return render_template("cib.html")

    elif request.method == "POST":
        connect = False
        body = request.form
        username = body.get("username")
        password = body.get("password")

        try:
            scrap = CIBScrap(username, password)
            scrap.scrap()
            connect = True
        except Exception:
            abort(400)

        if connect:
            return jsonify({"success": True}), 200
        else:
            return abort(400)


@app.route("/nbe", methods=["GET", "POST"])
def nbe():
    if request.method == "GET":
        return render_template("nbe.html")
    elif request.method == "POST":
        connect = False
        body = request.form
        username = body.get("username")
        password = body.get("password")
        card_type = body.get("type")
        print(f"{username} - {password} - {card_type}")
        try:
            scrap = NBEScrap(username, password, card_type)
            scrap.scrap()
            connect = True
        except Exception:
            abort(400)

        if connect:
            print("===>> GETTING DATA")
            data = scrap.get_data()
            return (
                jsonify([data]),
                200,
            )
        else:
            return abort(400)


@app.route("/misr", methods=["GET", "POST"])
def misr():
    if request.method == "GET":
        return render_template("misr.html")
    elif request.method == "POST":
        connect = False
        body = request.form
        username = body.get("username")
        password = body.get("password")

        try:
            scrap = MISRScrap(username, password)
            scrap.scrap()
            connect = True
        except Exception:
            abort(400)

        if connect:
            return jsonify({"success": True}), 200
        else:
            return abort(400)


@app.route("/mashraq", methods=["GET", "POST"])
def mashraq():
    if request.method == "GET":
        return render_template("mashraq.html")
    elif request.method == "POST":
        connect = False
        body = request.form
        username = body.get("username")
        password = body.get("password")

        try:
            scrap = MASHRAQScrap(username, password)
            scrap.scrap()
            connect = True
        except Exception:
            abort(400)

        if connect:
            return jsonify({"success": True}), 200
        else:
            return abort(400)


@app.route("/hsbc", methods=["GET", "POST"])
def hsbc():
    if request.method == "GET":
        return render_template("hsbc.html")
    elif request.method == "POST":
        connect = False
        body = request.form
        username = body.get("username")
        password = body.get("password")
        answer = body.get("answer")

        try:
            scrap = HSBCScrap(username, password)
            scrap.scrap(answer)
            connect = True
        except Exception:
            abort(400)

        if connect:
            return jsonify({"success": True}), 200
        else:
            return abort(400)


@app.route("/alex", methods=["GET", "POST"])
def alex():
    if request.method == "GET":
        return render_template("alex.html")
    elif request.method == "POST":
        connect = False
        body = request.form
        username = body.get("username")
        password = body.get("password")

        try:
            scrap = ALEXScrap(username, password)
            scrap.scrap()
            connect = True
        except Exception:
            abort(400)

        if connect:
            return jsonify({"success": True}), 200
        else:
            return abort(400)


@app.route("/hdb", methods=["GET", "POST"])
def hdb():
    if request.method == "GET":
        return render_template("hdb.html")
    elif request.method == "POST":
        connect = False
        body = request.form
        username = body.get("username")
        password = body.get("password")

        try:
            scrap = HDBScrap(username, password)
            scrap.scrap()
            connect = True
        except Exception:
            abort(400)

        if connect:
            return jsonify({"success": True}), 200
        else:
            return abort(400)


@app.route("/aib", methods=["GET", "POST"])
def aib():
    if request.method == "GET":
        return render_template("aib.html")
    elif request.method == "POST":
        connect = False
        body = request.form
        username = body.get("username")
        password = body.get("password")

        try:
            scrap = AIBScrap(username, password)
            scrap.scrap()
            connect = True
        except Exception:
            abort(400)

        if connect:
            return jsonify({"success": True}), 200
        else:
            return abort(400)


@app.route("/audi", methods=["GET", "POST"])
def audi():
    if request.method == "GET":
        return render_template("audi.html")
    elif request.method == "POST":
        connect = False
        body = request.form
        username = body.get("username")
        password = body.get("password")

        try:
            scrap = AUDIScrap(username, password)
            scrap.scrap()
            connect = True
        except Exception:
            abort(400)

        if connect:
            return jsonify({"success": True}), 200
        else:
            return abort(400)


@app.route("/fab", methods=["GET", "POST"])
def fab():
    if request.method == "GET":
        return render_template("fab.html")
    elif request.method == "POST":
        connect = False
        body = request.form
        username = body.get("username")
        password = body.get("password")

        try:
            scrap = FABScrap(username, password)
            scrap.scrap()
            connect = True
        except Exception:
            abort(400)

        if connect:
            print("===>> START DATA:")
            data = scrap.get_data()
            return (
                jsonify([data]),
                200,
            )
        else:
            return abort(400)


"""
first end-point to call
check jwt token if it still valid
@TODO make use of it with the front-end to make user skip login page
@TODO change it to 'GET' request with front-end
"""


@app.route("/authentication", methods=["POST"])
@check_auth
def authentication_check(msg):
    return jsonify({"msg": msg})


"""
testing end-point to check server is up
"""


"""
take user name and password for bank account and use selenium api to check

we return username and password in response to follow RESTful API Principle of not saving any data on server (stateless)
we will reuse username and password in generating user_id

@params: username , password
returns: username, password, captcha img src link
"""

"""
    user_id = request.args.get('user_id')

    if user_id:
        user_data = UserData.query.filter_by(user_id=user_id).first()

        if user_data:
            return jsonify({'user_id': user_data.user_id, 'data': user_data.data})
        else:
            return jsonify({'message': 'User data not found'})
    else:
        return jsonify({'message': 'Please provide a user_id'})

"""

"""
end-point to handle captcha and store data in db

it takes captcha code from user and username and password
it validate captcha using selenium api if true it encrypt user's data with user_id and store it in db

@params: username, password, captcha code
returns: jwt-token
"""


@app.route("/cap", methods=["POST"])
def cap():
    try:
        body = request.get_json()
        cap = body.get("cap")
        username = body.get("username")
        password = body.get("password")
        print(f"===> Cap: {cap} - User: {username} - Pass: {password}")
        s = CIBScrap(username, password)
        if s.enter_cap(cap):
            d = s.get_balance_and_acc_num()
            print(d)  # check the console output to see the account details
            balance = d[0]
            acc_num = d[1]
            data = s.get_profile_info()
            print(data)  # check the console output to see the account details
            s.download_records()
            print("downloading records ending")
            user_id = f.get_user_ID(username, password, acc_num).decode("utf-8")
            jwt = cl.create_jwt({"userID": user_id})

            """
            then we check if user_id exist in our DB 
            if exist = True
                we update our DB
            else
                we create new user 

            """
            user_db = User.query.filter_by(user_id=user_id).one_or_none()
            if user_db is None:
                """
                create user
                """
                user_data = [username, password]
                enc_user_data = f.encrypt(user_data, user_id)
                # remove user's credentials
                user = User(
                    user_id=user_id,
                    account_num=acc_num,
                    balance=balance,
                    phone_num=data[1],
                    address=data[5],
                    email=data[3],
                )
                user.insert()
                print("user added")
            else:
                """
                update user data
                """
                user_db.balance = balance
                user_db.address = data[5]
                user_db.email = data[3]
                user_db.phone_num = data[1]

                user_db.update()
                print("user updated")
            return jsonify({"success": True, "jwt": jwt}), 200
        else:
            return jsonify({"success": False}), 200
    except Exception as e:
        print("error ", str(e))
        abort(400)


"""
refresh JWT token

@TODO cross reference user's data to refresh token 
@TODO it should take use data like phone number to cross reference with. (front-end) 
returns: JWT token 
"""


@app.route("/token", methods=["POST"])
def update_token():
    try:
        # jwt in header not body
        token = get_token_auth_header()

        # get userID to get user's data from DB for cross reference
        # and make cross reference with the data in db
        userID = cl.get_token_data(token)["userID"]
        jwt = cl.refresh_token(token)
        return jsonify({"success": True, "jwt": jwt}), 200
    except:
        abort(400)


"""
data end-point for retrieving data from the db

first it check jwt if valid it returns data

@params: JWT in Header
returns: User_id, account number, Phone number, address, email, balance
"""


@require_auth
@app.route("/account_details", methods=["GET"])
def get_account_details():
    users = User.query.all()
    user_data = []

    for user in users:
        user_details = {
            "id": user.id,
            "username": user.username,
            "bank_details": [],
        }

        for bank_detail in user.details:
            bank_detail_data = {
                "id": bank_detail.id,
                "user_address": bank_detail.user_address,
                "user_branch": bank_detail.user_branch,
                "customer_id": bank_detail.customer_id,
                "account_number": bank_detail.account_number,
                "currency": bank_detail.currency,
                "iban": bank_detail.iban,
                "month": bank_detail.month,
            }
            user_details["bank_details"].append(bank_detail_data)

        user_data.append(user_details)

    return jsonify(user_data)


@require_auth
@app.route("/account_transactions", methods=["GET"])
def get_account_transactions():
    users = User.query.all()
    user_list = []

    for user in users:
        user_data = {
            "id": user.id,
            "username": user.username,
            "transactions": [],
        }

        transactions = user.transactions
        for transaction in transactions:
            transaction_data = {
                "id": transaction.id,
                "posting_date": transaction.posting_date,
                "value_date": transaction.value_date,
                "particulars": transaction.particulars,
                "debit": transaction.debit,
                "credit": transaction.credit,
                "balance": transaction.balance,
            }
            user_data["transactions"].append(transaction_data)

        user_list.append(user_data)

    return jsonify(user_list)


@app.route("/data", methods=["POST"])
@require_auth
def return_data(payload):
    try:
        body = payload["data"]
        user_id = body.get("userID")
        if user_id is None:
            abort(400)
        user_db = User.query.filter_by(user_id=user_id).one_or_none()
        if user_db is None:
            return jsonify({"success": False, "message": "user is not exist"})
        else:
            pass
            # data = [user_db.enc_username, user_db.enc_password]
            # de_data = f.decrypt(data, user_id)
            # s.download_records()
        return jsonify(
            {
                "success": True,
                "UserID": user_db.user_id,
                "AccountNum": user_db.account_num,
                "PhoneNum": user_db.phone_num,
                "Address": user_db.address,
                "Email": user_db.email,
                "Balance": user_db.balance,
            }
        )
    except:
        abort(400)


"""
validate OTP end-point

it handle sending OTP to user and validate it

@params: phonenumber: for sending to
@params: OTP : to validate (optional)
returns: OTP, status or phoneNumber, status
"""


@app.route("/phone", methods=["POST"])
def phone_validate():
    try:
        body = request.get_json()
        if body.get("OTP"):
            OTP = body.get("OTP")
            phone_num = body.get("phonenumber")
            phone_num = "+20" + phone_num
            status = pv.check_OTP(OTP, phone_num)
            return jsonify({"success": True, "OTP": OTP, "status": status})
        else:
            phone_num = body.get("phonenumber")
            phone_num = "+20" + phone_num
            status = pv.sendOTP(phone_num)
            return jsonify({"success": True, "phone": phone_num, "status": status})
    except:
        abort(400)


"""
error handlers
"""


@app.errorhandler(404)
def not_found(error):
    return (
        jsonify({"success": False, "error": 404, "message": "resource not found"}),
        404,
    )


@app.errorhandler(400)
def bad_request(error):
    return jsonify({"success": False, "error": 400, "message": "Bad Request"}), 400


@app.errorhandler(422)
def unprocessable(error):
    return (
        jsonify({"success": False, "error": 422, "message": "Unprocessable Request"}),
        422,
    )


@app.errorhandler(401)
def unauthorized(error):
    return (
        jsonify({"success": False, "error": 401, "message": "Unauthorized Request"}),
        401,
    )
