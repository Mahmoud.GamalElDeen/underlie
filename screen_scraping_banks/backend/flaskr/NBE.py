import time

import tabula
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from .scrap import Scrap


class NBEScrap(Scrap):
    url = "https://www.alahlynet.com.eg/?page=home"
    profile_setting_url = ""
    bank = "NBE"

    def scrap(self):
        try:
            self.browser.get(self.url)
            wait = WebDriverWait(self.browser, 30)
            # login in
            login_el = wait.until(
                EC.presence_of_element_located(
                    (
                        By.XPATH,
                        "//*[@id='dbContainer']/obdxcomponent/div/div[2]/section/obdxcomponent/div[1]/div[1]/div/img",
                    )
                )
            )
            self.browser.execute_script("arguments[0].click();", login_el)
            time.sleep(5)

            # user id
            user_id_el = wait.until(
                EC.presence_of_element_located(
                    (
                        By.XPATH,
                        "//*[@id='maincontent']/div[2]/div/div/div/obdxcomponent/div/div[2]/div/div[2]/div/div[2]/input",
                    )
                )
            )
            user_id_el.send_keys(self.username)
            user_id_el.send_keys(Keys.ENTER)
            time.sleep(2)

            # password
            password_el = wait.until(
                EC.presence_of_element_located(
                    (By.XPATH, "//*[@id='login_password']")
                )
            )
            password_el.send_keys(self.password)
            password_el.send_keys(Keys.ENTER)
            time.sleep(10)

        except Exception as e:
            print(e)
            raise e

        # ========> STEP 2 <======== #
        print("# ========> STEP 2 <======== #")
        if str(self.type) == "credit":
            self.__navigate_to_account_details_credit()

        elif str(self.type) == "debit":
            self.__navigate_to_account_details_debit()

        self.browser.close()
        self.browser.quit()

    def __navigate_to_account_details_debit(self):
        try:
            account_btn = self.browser.find_element(
                By.XPATH, "//div[contains(text(),'Accounts')]"
            )
            account_btn.click()
            time.sleep(3)

            egp_account_btn = self.browser.find_element(
                By.XPATH,
                "//body/obdx-dashboard[1]/obdxcomponent[1]/div[1]/div[1]/div[2]/main[1]/dashboard-container[1]/obdxcomponent[1]/div[1]/div[2]/div[2]/section[1]/obdxcomponent[1]/div[1]/div[1]/div[1]/flip-account[1]/obdxcomponent[1]/div[1]/oj-list-view[1]/ul[1]/li[1]/div[1]/div[1]/div[3]/a[1]/div[1]",
            )
            egp_account_btn.click()
            time.sleep(3)

            account_details_btn = self.browser.find_element(
                By.XPATH,
                "//oj-menu[@id='CSA0-container']//oj-option[@id='demand-deposit-transactions']",
            )
            self.browser.execute_script("arguments[0].click();", account_details_btn)
            time.sleep(2)

            account_activity_btn2 = self.browser.find_element(
                By.ID, "demand-deposit-transactions"
            )
            account_activity_btn2.click()
            time.sleep(2)

            download_transactions_btn = self.browser.find_element(
                By.XPATH,
                "//body/obdx-dashboard[1]/obdxcomponent[1]/div[1]/div[1]/div[2]/main[1]/div[2]/div[1]/manage-accounts[1]/obdxcomponent[1]/div[1]/div[1]/div[1]/account-transactions[1]/obdxcomponent[1]/div[1]/oj-validation-group[1]/div[1]/div[1]/page-section[1]/obdxcomponent[1]/div[1]/div[1]/div[1]/div[1]/oj-form-layout[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/oj-menu-button[1]/button[1]",
            )
            self.browser.execute_script(
                "arguments[0].click();", download_transactions_btn
            )
            download_transactions_pdf_btn = self.browser.find_element(
                By.XPATH, "//a[@id='ui-id-289']"
            )
            self.browser.execute_script(
                "arguments[0].click();", download_transactions_pdf_btn
            )

            download_transactions_pdf_btn.click()

        except Exception as e:
            print(e)
            raise e

    def __navigate_to_account_details_credit(self):
        try:
            wait = WebDriverWait(self.browser, 30)
            credit_btn = wait.until(
                EC.presence_of_element_located(
                    (
                        By.XPATH,
                        "/html/body/obdx-dashboard/obdxcomponent/div/div[1]/div[2]/main/dashboard-container/obdxcomponent/div/div[2]/div[2]/section/obdxcomponent/div/div[2]/oj-list-view/ul/li[4]/a/div/div[1]/div",
                    )
                )
            )
            self.browser.execute_script("arguments[0].click();", credit_btn)
            self.browser.implicitly_wait(3)
            more_action = self.browser.find_element(
                By.XPATH,
                "/html/body/obdx-dashboard/obdxcomponent/div/div[1]/div[2]/main/dashboard-container/obdxcomponent/div/div[2]/div[2]/section/obdxcomponent/div/div[1]/div/flip-account/obdxcomponent/div/oj-list-view/ul/li[1]/div/div/div[3]/a/div",
            )
            self.browser.execute_script("arguments[0].click();", more_action)
            time.sleep(1)
            statement_btn = self.browser.find_element(
                By.XPATH, "/html/body/div[1]/div/oj-menu/oj-option[2]/a/span"
            )
            self.browser.execute_script("arguments[0].click();", statement_btn)
            time.sleep(1)
        except Exception as e:
            print(e)
            raise e

        # ========> STEP 3 <======== #
        print("# ========> STEP 3 <======== #")
        self.__download_records()

    def get_data(self):
        files_path = self.get_dir_files()
        # for test we will get only the first file
        f = files_path[0]

        if str(self.type) == "credit":
            d = self.__credit_account_details(f)
            t = self.__credit_account_transactions(f)

        elif str(self.type) == "debit":
            d = self.__debit_account_details(f)
            t = self.__debit_account_transactions(f)
        self.delete_files()
        return {
            "user": self.username,
            "bank_details": d,
            "bank_transactions": t,
        }

    def __download_records(self):
        try:
            select_year_btn = self.browser.find_element(
                By.XPATH,
                "/html/body/obdx-dashboard/obdxcomponent/div/div[1]/div[2]/main/div[2]/div/manage-accounts/obdxcomponent/div/div/div/obdxcomponent/div[2]/div[4]/div/div[1]/div/oj-select-one/div/div[1]/div/div",
            )
            select_year_btn.click()
            time.sleep(1)
            select_year = self.browser.find_element(
                By.XPATH, "/html/body/div[1]/div/div/ul/li[2]"
            )
            select_year.click()
            time.sleep(1)

            select_month_btn = self.browser.find_element(
                By.XPATH,
                "/html/body/obdx-dashboard/obdxcomponent/div/div[1]/div[2]/main/div[2]/div/manage-accounts/obdxcomponent/div/div/div/obdxcomponent/div[2]/div[4]/div/div[2]/div/oj-select-one/div/div[1]/div/div",
            )
            select_month_btn.click()
            time.sleep(1)

            select_month = self.browser.find_element(
                By.XPATH,
                "/html/body/div[1]/div/div/ul/li[2]",
            )
            select_month.click()
            time.sleep(1)

            download_btn = self.browser.find_element(By.ID, "autoPayCancelButton")
            self.browser.execute_script("arguments[0].click();", download_btn)
            time.sleep(10)

        except Exception as e:
            raise e

    def __debit_account_details(self, file_path):
        data = tabula.read_pdf(file_path, pages="all")
        data = data[0]
        details = []
        details.append(
            {
                "customer_id": str(data.get("Customer Id")[0]),
                "account_number": str(data.get("Account Number")[0]),
                "currency": data.get("Account Currency")[0],
                "month": data.get("From Date")[0],
            }
        )
        return details

    def __debit_account_transactions(self, file_path):
        data = tabula.read_pdf(file_path, pages="all")
        data_list = []
        for row in data[1:]:
            data_dict = row.to_dict(orient="records")
            data_list.extend(data_dict)
        return data_list

    def __credit_account_details(self, file_path):
        data = tabula.read_pdf(file_path, pages="all")
        data = data[0]
        details = []
        details.append(
            {
                "customer_id": "",
                "account_number": str(data.get("Unnamed: 0")[0]),
                "currency": "EGY",
                "month": data.get("Unnamed: 2")[4],
            }
        )

        return details

    def __credit_account_transactions(self, file_path):
        data = tabula.read_pdf(file_path, pages="all")
        data_list = []
        for row in data[1:]:
            for d in row.values:
                data_list.append(
                    {
                        "posting_date": d[0],
                        "value_date": d[1],
                        "particulars": d[2],
                        "debit": d[3],
                        "credit": d[4],
                        "balance": d[5],
                    }
                )

        return data_list
