import time
from pprint import pprint

import tabula
from selenium.webdriver.common.by import By

from .scrap import Scrap


class ALEXScrap(Scrap):
    url = "https://www.alexbank.com/retail/mobile-internet-banking.html#login"
    profile_setting_url = ""
    bank = "ALEX"

    def scrap(self):
        try:
            self.browser.get(self.url)
            self.browser.implicitly_wait(5)

            log_on_el = self.browser.find_element(By.CLASS_NAME, "sectionCta__label")
            self.browser.execute_script("arguments[0].click();", log_on_el)
            time.sleep(1)

            user_name_el = self.browser.find_element(By.ID, "username")
            user_name_el.send_keys(self.username)

            password_el = self.browser.find_element(By.ID, "password")
            password_el.send_keys(self.password)

            submit_btn = self.browser.find_element(
                By.XPATH,
                "/html/body/header/div/div[2]/div/div/div[1]/div/div[2]/div/div/div[1]/div/div/div/div/div/div/form/button",
            )
            submit_btn.click()
            time.sleep(5)
        except Exception as e:
            raise e
        self.browser.close()
        self.browser.quit()
