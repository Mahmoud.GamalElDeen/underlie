import time

import tabula
from selenium.webdriver.common.by import By

from .scrap import Scrap


class FABScrap(Scrap):
    url = "https://online.fabmisr.com.eg/Default?sID=2237012"
    profile_setting_url = ""
    bank = "FAB"

    def scrap(self):
        try:
            self.browser.get(self.url)
            self.browser.implicitly_wait(10)
            user_id_el = self.browser.find_element(By.ID, "txtName")
            user_id_el.send_keys(self.username)
            time.sleep(1)
            password_el = self.browser.find_element(By.ID, "txtStep1Pass_fake")
            password_el.send_keys(self.password)
            time.sleep(1)
            submit_btn = self.browser.find_element(By.ID, "SubmitStep1")
            submit_btn.click()
            time.sleep(1)

            # ========> STEP 2 <======== #
            self.__navigate_to_account_details()
            # ========> STEP 3 <======== #
            self.__download_records()

        except Exception as e:
            raise e
        self.browser.close()
        self.browser.quit()

    def __navigate_to_account_details(self):
        try:
            account_el = self.browser.find_element(
                By.XPATH,
                "/html/body/form/div[4]/div/div[1]/div[2]/div[1]/ul[1]/li[2]/a/span[3]",
            )
            self.browser.execute_script("arguments[0].click();", account_el)
            time.sleep(1)
            current_el = self.browser.find_element(By.ID, "menuId_1068")
            current_el.click()
            time.sleep(1)

            iframe = self.browser.find_element(By.CLASS_NAME, "#frame_content")
            self.browser.switch_to.frame(iframe)
            card_el = self.browser.find_element(
                By.XPATH,
                "/html/body/form/div[3]/div/div[2]/div[1]/a/span[3]/span[1]/span[2]",
            )

            self.browser.execute_script("arguments[0].click();", card_el)
            time.sleep(1)
            activities_el = self.browser.find_element(
                By.ID, "ctl00_ContentPlaceHolder1_btnAccountTransactions"
            )
            activities_el.click()
            time.sleep(1)
        except Exception as e:
            raise e

    def __download_records(self):
        try:
            dropdown = self.browser.find_element(
                By.XPATH, "//div[@class='select2-container select']"
            )
            dropdown.click()
            time.sleep(2)
            option = self.browser.find_element(
                By.XPATH, "/html/body/div[3]/div[2]/ul/li[4]/div"
            )
            option.click()
            time.sleep(2)
            download_el = self.browser.find_element(
                By.XPATH, "/html/body/form/div[3]/div[1]/div[3]/div[1]/div[2]/a[1]"
            )
            download_el.click()
            time.sleep(1)
            to_pdf_el = self.browser.find_element(
                By.ID,
                "ctl00_ContentPlaceHolder1_AccountTransactionExport_ExportFormatToPdf",
            )
            to_pdf_el.click()
            time.sleep(5)
        except Exception as e:
            raise e

    def get_data(self):
        files_path = self.get_dir_files()
        # for test we will get only the first file
        f = files_path[0]
        d = self.__account_details(f)
        t = self.__account_transactions(f)
        self.delete_files()
        return {
            "user": self.username,
            "bank_details": d,
            "bank_transactions": t,
        }

    def __account_details(self, file_path):
        data = tabula.read_pdf(file_path, pages="all")
        details = []
        for row in data[:1]:
            for d in row.values:
                details.append(
                    {
                        "customer_id": "",
                        "account_number": d[0],
                        "currency": d[1],
                        "month": "",
                    }
                )

        return details

    def __account_transactions(self, file_path):
        data = tabula.read_pdf(file_path, pages="all")
        details = []
        for row in data[1:]:
            for d in row.values:
                if any(
                    str(value) == "nan"
                    for value in [d[0], d[1], d[4], d[6], d[7], d[8]]
                ):
                    continue
                details.append(
                    {
                        "posting_date": d[0],
                        "value_date": d[1],
                        "particulars": d[4],
                        "debit": d[6],
                        "credit": d[7],
                        "balance": d[8],
                    }
                )

        return details
