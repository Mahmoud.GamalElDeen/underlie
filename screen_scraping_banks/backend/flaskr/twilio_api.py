# Download the helper library from https://www.twilio.com/docs/python/install
import os
from twilio.rest import Client


# Your Account Sid and Auth Token from twilio.com/console
# and set the environment variables. See http://twil.io/secure
class phoneValidation :
    def __init__(self):
        self.account_sid = "ACc8b1099a0ca4ecc986bbe28c1b468591"
        self.auth_token = "d94d0d17869fffce47032685a18705cf"
        self.service = "VA66d73fac4c58a59ab70c812707bf97b6"
        self.client = Client(self.account_sid, self.auth_token)

    def sendOTP(self, phone):
        verification = self.client.verify \
                        .services(self.service) \
                        .verifications \
                        .create(to=phone, channel='sms')
        return verification.status

    def check_OTP(self,OTP, phone):
        verification_check = self.client.verify \
                                .services(self.service) \
                                .verification_checks \
                                .create(to=phone, code=OTP)

        return verification_check.status
