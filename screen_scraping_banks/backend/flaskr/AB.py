import pytest
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import tabula
from selenium.webdriver.common.by import By

import csv
import json

from .scrap import Scrap


class TestArabBank():
    def setup_method(self, method):
        self.driver = webdriver.Chrome()
        self.vars = {}

    def teardown_method(self, method):
        self.driver.quit()

    def wait_for_window(self, timeout=2):
        time.sleep(round(timeout / 1000))
        wh_now = self.driver.window_handles
        wh_then = self.vars["window_handles"]
        if len(wh_now) > len(wh_then):
            return set(wh_now).difference(set(wh_then)).pop()

    def test_arabBank(self):
        # Test name: ِArab Bank
        # Step # | name | target | value
        # 1 | open | https://www.arabbank.com.eg/ar |
        self.driver.get("https://www.arabbank.com.eg/ar")
        # 2 | setWindowSize | 1552x832 |
        self.driver.set_window_size(1552, 832)
        # 3 | click | css=.rightHeading:nth-child(3) .userBtn |
        self.driver.find_element(By.CSS_SELECTOR, ".rightHeading:nth-child(3) .userBtn").click()
        # 4 | click | linkText=عربي أون لاين |
        self.vars["window_handles"] = self.driver.window_handles
        # 5 | selectWindow | handle=${win7798} |
        self.driver.find_element(By.LINK_TEXT, "عربي أون لاين").click()
        # 6 | click | id=focusObj |
        self.vars["win7798"] = self.wait_for_window(2000)
        # 7 | type | id=focusObj | ahmedhesham1
        self.driver.switch_to.window(self.vars["win7798"])
        # 8 | type | id=captchaImageID | 8598
        self.driver.find_element(By.ID, "focusObj").click()
        # 9 | click | css=#enterSubmitUserName > .ui-button-text |
        self.driver.find_element(By.ID, "focusObj").send_keys("ahmedhesham1")
        # 10 | click | id=password |
        self.driver.find_element(By.ID, "captchaImageID").send_keys("8598")
        # 11 | type | id=password | 9874562$zZ
        self.driver.find_element(By.CSS_SELECTOR, "#enterSubmitUserName > .ui-button-text").click()
        # 12 | click | css=#loginLoginForm > .ui-button-text |
        self.driver.find_element(By.ID, "password").click()
        # 13 | click | id=activationNumber |
        self.driver.find_element(By.ID, "password").send_keys("9874562$zZ")
        # 14 | type | id=activationNumber | 493854
        self.driver.find_element(By.CSS_SELECTOR, "#loginLoginForm > .ui-button-text").click()
        # 15 | sendKeys | id=activationNumber | ${KEY_ENTER}
        self.driver.find_element(By.ID, "activationNumber").click()
        # 16 | selectFrame | index=0 |
        self.driver.find_element(By.ID, "activationNumber").send_keys("493854")
        # 17 | click | id=acctMgtLinkMenuId |
        self.driver.find_element(By.ID, "activationNumber").send_keys(Keys.ENTER)
        # 18 | click | css=#LastXTransactionsId-menu > .ui-selectmenu-status |
        self.driver.switch_to.frame(0)
        # 19 | click | id=ui-selectmenu-item-706 |
        self.driver.find_element(By.ID, "acctMgtLinkMenuId").click()
        # 20 | click | css=#viewAccountHistoryID2 > .ui-button-text |
        self.driver.find_element(By.CSS_SELECTOR, "#LastXTransactionsId-menu > .ui-selectmenu-status").click()
        # 21 | click | css=#consumerAccountsDropDown-menu > .ui-selectmenu-status |
        self.driver.find_element(By.ID, "ui-selectmenu-item-706").click()
        # 22 | click | id=ui-selectmenu-item-136 |
        self.driver.find_element(By.CSS_SELECTOR, "#viewAccountHistoryID2 > .ui-button-text").click()
        # 23 | click | css=#viewAccountHistoryID2 > .ui-button-text |
        self.driver.find_element(By.CSS_SELECTOR, "#consumerAccountsDropDown-menu > .ui-selectmenu-status").click()
        # 24 | click | css=.ui-pg-selbox |
        self.driver.find_element(By.ID, "ui-selectmenu-item-136").click()
        # 25 | click | css=#anchor_1901122330 > .ui-button-text |
        self.driver.find_element(By.CSS_SELECTOR, "#viewAccountHistoryID2 > .ui-button-text").click()
        # 26 | click | css=#submitExportButonId > .ui-button-text |
        self.driver.find_element(By.CSS_SELECTOR, ".ui-pg-selbox").click()
        # 27 | click | css=#anchor_1901122330 > .ui-button-text |
        self.driver.find_element(By.CSS_SELECTOR, "#anchor_1901122330 > .ui-button-text").click()
        # 28 | click | css=.mainfont:nth-child(3) > input |
        self.driver.find_element(By.CSS_SELECTOR, "#submitExportButonId > .ui-button-text").click()
        # 29 | click | css=#submitExportButonId > .ui-button-text |
        self.driver.find_element(By.CSS_SELECTOR, "#anchor_1901122330 > .ui-button-text").click()
        # 30 | click | id=anchor_1381981453 |
        self.driver.find_element(By.CSS_SELECTOR, ".mainfont:nth-child(3) > input").click()
        # 31 | click | css=.ui-selectmenu-status |
        self.driver.find_element(By.CSS_SELECTOR, "#submitExportButonId > .ui-button-text").click()
        # 32 | click | id=ui-selectmenu-item-67 |
        self.driver.find_element(By.ID, "anchor_1381981453").click()
        # 33 | click | css=.ui-pg-selbox |
        self.driver.find_element(By.CSS_SELECTOR, ".ui-selectmenu-status").click()
        # 34 | click | css=#exportButton > .ui-button-text |
        self.driver.find_element(By.ID, "ui-selectmenu-item-67").click()
        # 35 | click | css=#submitExportButonId > .ui-button-text |
        self.driver.find_element(By.CSS_SELECTOR, ".ui-pg-selbox").click()
        # 36 | click | css=#exportButton > .ui-button-text |
        self.driver.find_element(By.CSS_SELECTOR, "#exportButton > .ui-button-text").click()
        # 37 | click | css=.mainfont:nth-child(3) > input |
        self.driver.find_element(By.CSS_SELECTOR, "#submitExportButonId > .ui-button-text").click()
        # 38 | click | css=#submitExportButonId > .ui-button-text |
        self.driver.find_element(By.CSS_SELECTOR, "#exportButton > .ui-button-text").click()
        self.driver.find_element(By.CSS_SELECTOR, ".mainfont:nth-child(3) > input").click()
        self.driver.find_element(By.CSS_SELECTOR, "#submitExportButonId > .ui-button-text").click()

    def __account_details(self, file_path):
        data = tabula.read_pdf(file_path, pages="all")
        details = []
        for row in data[:1]:
            for d in row.values:
                details.append(
                    {
                        "customer_id": "",
                        "account_number": d[0],
                        "currency": d[1],
                        "month": "",
                    }
                )

        return details

    def __account_transactions(self, file_path):
        # Specify the correct file path
        csv_file_path = file_path

        # Create a list to store the rows as dictionaries
        data = []

        # Open the CSV file with the appropriate encoding
        with open(csv_file_path, 'r', encoding='utf-16') as csvfile:
            csvreader = csv.DictReader(csvfile, delimiter='\t')
            for row in csvreader:
                data.append(row)

        # Convert the list of dictionaries to JSON format
        json_data = json.dumps(data, ensure_ascii=False, indent=4)

        # # Print the JSON data
        # with open('organized_data.json', 'w', encoding='utf-8') as json_file:
        #     json_file.write(json_data)
        #
        # #                     DONE (CSV)                                 #
        details = json_data
        return details
