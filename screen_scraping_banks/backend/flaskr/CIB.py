import os
import time

import pandas as pd
import PIL
import pytesseract
import xlrd
from PIL import Image
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select, WebDriverWait

from .scrap import Scrap


class CIBScrap(Scrap):
    url = "https://ebanking.cibeg.com/CIBInternet240/#/"
    profile_setting_url = (
        "https://ebanking.cibeg.com/CIBInternet240/#/root/profileSettings"
    )
    bank = "CIB"

    def scrap(self):
        try:
            self.browser.get(self.url)
            time.sleep(2)
            self.loading()
            username_el = self.browser.find_element(By.NAME, "username")
            password_el = self.browser.find_element(By.NAME, "password")
            submit_btn_el = self.browser.find_element(By.ID, "BW_button_802163")

            time.sleep(1)
            username_el.send_keys(self.username)
            password_el.send_keys(self.password)

            time.sleep(1)
            submit_btn_el.click()

            time.sleep(1)
            self.loading()

            caption = self.browser.find_element(By.CSS_SELECTOR, ".login_captcha_image")
            src = caption.get_attribute("src")
            self.browser.find_element(By.TAG_NAME, "body").send_keys(Keys.CONTROL + "t")

            # open new tab
            self.browser.execute_script("window.open('');")
            self.browser.switch_to.window(self.browser.window_handles[1])
            self.browser.get(src)
            time.sleep(1)

            # ========> STEP 2 <======== #
            src = self.get_src()
            # ========> STEP 3 <======== #
            self.cap_parse(src)

            # ========> STEP 4 <======== #
            # compelete the logic

        except (Exception, WebDriverException) as e:
            print("===>> Selenium error occurred:")
            print(e)

    def get_src(self):
        try:
            os.makedirs(self.download_default_directory, exist_ok=True)
            self.browser.save_screenshot(
                f"{self.download_default_directory}/screenshot.png"
            )
            self.browser.find_element(By.TAG_NAME, "body").send_keys(Keys.CONTROL + "w")
            time.sleep(1)

            src = self.get_dir_files()[0]
            return src
        except WebDriverException as e:
            print("===>> Selenium error occurred:")
            print(e)

    def cap_parse(self, src):
        image = Image.open(src)
        if os.name == "nt":
            pytesseract_folder = r"C:\Program Files (x86)\Tesseract-OCR\tesseract"
            pytesseract.pytesseract.tesseract_cmd = pytesseract_folder
            cropped_image = image.crop((614, 278, 752, 325))
        else:
            cropped_image = image.crop((331, 276, 469, 324))
            cropped_image.save(f"{src[:-15]}/cropped_image.png")

        try:
            text = pytesseract.image_to_string(cropped_image)
            text.replace(" ", "")
            print("===>>> TEXT:", text)

            self.browser.switch_to.window(self.browser.window_handles[0])
            caption_input = self.browser.find_element(By.ID, "captchaText")
            caption_input.send_keys(text + Keys.ENTER)
            time.sleep(1)
            self.delete_files()
            self.loading()

        except (Exception, WebDriverException) as e:
            print("===>> Selenium error occurred:")
            print(e)

    def get_balance_and_acc_num(self):
        try:
            balance_el = WebDriverWait(self.browser, timeout=100).until(
                lambda d: d.find_element(By.CSS_SELECTOR, ".acc_value")
            )
            balance = balance_el.get_attribute("innerText")
            acc_name = WebDriverWait(self.browser, timeout=100).until(
                lambda d: d.find_element(By.CSS_SELECTOR, ".acc_name")
            )
            acc_num = acc_name.find_element(
                By.CSS_SELECTOR, ".acc_value"
            ).get_attribute("innerText")
            print(balance, acc_num)
        except WebDriverException as e:
            print("===>> Selenium error occurred:")
            print(e)
        return balance, acc_num

    def get_profile_info(self):
        """
        TBD check if url changed
        current_url = self.browser.current_url
        if
        """
        try:
            self.browser.get(self.profile_setting_url)
            profile_info_btn = (
                WebDriverWait(self.browser, timeout=10)
                .until(lambda d: d.find_element(By.CSS_SELECTOR, "#BW_input_217160"))
                .click()
            )
            info_box_ele = WebDriverWait(self.browser, timeout=10).until(
                lambda d: d.find_element(By.CSS_SELECTOR, "#fodal")
            )
            labels = info_box_ele.find_element(By.CSS_SELECTOR, "label")
            print(labels)

            data = []
            for label in labels:
                data.append(label.get_attribute("innerText"))
            # self.browser.get(self.url)
        except WebDriverException as e:
            print("===>> Selenium error occurred:")
            print(e)
        return data

    """
    helping function : will keep running till loading complete
    """

    def loading(self):
        try:
            time.sleep(1)
            spinner_loading = True
            while spinner_loading:
                spinner_ele = self.browser.find_element(
                    By.CSS_SELECTOR, ".global-spinner"
                )
                spinner_class = spinner_ele.get_attribute("class")
                if spinner_class.find("ng-hide") == -1:
                    spinner_loading = True
                    time.sleep(1)
                else:
                    spinner_loading = False
        except WebDriverException as e:
            print("===>> Selenium error occurred:")
            print(e)

    def download_records(self):
        self.loading()
        current_url = self.browser.current_url
        root = "root"
        if root not in current_url:
            return
        try:
            self.browser.get(
                "https://ebanking.cibeg.com/CIBInternet240/#/root/archivedAccountStatement"
            )
            time.sleep(1)
            select = Select(
                WebDriverWait(self.browser, timeout=10).until(
                    lambda d: d.find_element(By.CSS_SELECTOR, "#destination_account")
                )
            )
            time.sleep(2)
            select.select_by_index(1)
            time.sleep(4)
            iframe = self.browser.find_element(By.CSS_SELECTOR, "iframe")
            src = iframe.get_attribute("src")
            while src == "":
                time.sleep(1)
                iframe = self.browser.find_element(By.CSS_SELECTOR, "iframe")
                src = iframe.get_attribute("src")
            # open new tab
            self.browser.execute_script("window.open('');")
            # switch to the new tab
            self.browser.switch_to.window(self.browser.window_handles[1])
            # go to url
            self.browser.get(src)
            years_table = WebDriverWait(self.browser, timeout=10).until(
                lambda d: d.find_element(By.CSS_SELECTOR, "#Accordion1")
            )

            years_content = years_table.find_elements(
                By.CSS_SELECTOR, ".accordionContent"
            )
            monthes = years_content[0].find_elements(
                By.CSS_SELECTOR, "input[type=submit]"
            )
            i = 0
            monthes_len = len(monthes)
            for i in range(0, monthes_len):
                time.sleep(1)
                years_table = WebDriverWait(self.browser, timeout=10).until(
                    lambda d: d.find_element(By.CSS_SELECTOR, "#Accordion1")
                )
                years_content = years_table.find_elements(
                    By.CSS_SELECTOR, ".accordionContent"
                )
                monthes = years_content[0].find_elements(
                    By.CSS_SELECTOR, "input[type=submit]"
                )
                monthes[i].click()
                self.browser.execute_script(
                    "excel = document.querySelector('#ibtnExcel1');"
                )
                self.browser.execute_script("excel.click();")
                # wait = WebDriverWait(self.browser, 10)
                # excel_btn = wait.until(EC.element_to_be_clickable((By.ID, "#ibtnExcel1")))
                # # excel_btn = WebDriverWait(self.browser, timeout=10).until(
                #     # lambda d: d.find_element(By.CSS_SELECTOR,'#ibtnExcel1'))
                # excel_btn.click()
                time.sleep(1)
                i += 1
            # *******************************************************************
            if len(years_content) < 2:
                return
            years = []
            years.append(
                self.browser.find_element(By.CSS_SELECTOR, ".accordionHeaderSelected")
            )
            years.append(
                self.browser.find_elements(By.CSS_SELECTOR, ".accordionHeader")
            )
            y = years[1]
            y[0].click()
            time.sleep(1)
            years_table = WebDriverWait(self.browser, timeout=10).until(
                lambda d: d.find_element(By.CSS_SELECTOR, "#Accordion1")
            )
            years_content = years_table.find_elements(
                By.CSS_SELECTOR, ".accordionContent"
            )

            monthes = years_content[1].find_elements(
                By.CSS_SELECTOR, "input[type=submit]"
            )
            i = 0
            monthes_len = len(monthes)
            for i in range(0, monthes_len):
                years_table = WebDriverWait(self.browser, timeout=10).until(
                    lambda d: d.find_element(By.CSS_SELECTOR, "#Accordion1")
                )
                years_content = years_table.find_elements(
                    By.CSS_SELECTOR, ".accordionContent"
                )
                monthes = years_content[1].find_elements(
                    By.CSS_SELECTOR, "input[type=submit]"
                )
                monthes[i].click()
                self.browser.execute_script(
                    "excel = document.querySelector('#ibtnExcel1');"
                )
                self.browser.execute_script("excel.click();")
                print("excel.click();")
                # wait = WebDriverWait(self.browser, 10)
                # excel_btn = wait.until(EC.element_to_be_clickable((By.ID, "#ibtnExcel1")))
                # # excel_btn = WebDriverWait(self.browser, timeout=10).until(
                #     # lambda d: d.find_element(By.CSS_SELECTOR,'#ibtnExcel1'))
                # excel_btn.click()
                time.sleep(1)
                i += 1
            return 1
        except WebDriverException as e:
            print("===>> DRIVER ERROR:", e)


def set_full_address(value_1, value_2):
    return f"{value_1} {value_2}"


def extract_account_details(file_path):
    wb = xlrd.open_workbook(file_path, logfile=open(os.devnull, "w"))
    data = pd.read_excel(
        wb,
        usecols=[
            "Unnamed: 2",
            "Unnamed: 3",
            "Unnamed: 4",
            "Unnamed: 5",
            "Unnamed: 6",
            "Unnamed: 7",
            "Unnamed: 8",
            "Unnamed: 9",
            "Unnamed: 10",
            "Unnamed: 11",
            "Unnamed: 12",
            "Unnamed: 13",
            "Unnamed: 14",
            "Unnamed: 15",
            "Unnamed: 16",
            "Unnamed: 17",
            "Unnamed: 18",
            "Unnamed: 19",
            "Unnamed: 20",
            "Unnamed: 21",
            "Unnamed: 22",
            "Unnamed: 23",
            "Unnamed: 24",
            "Unnamed: 25",
        ],
    )
    data = data.head(15)
    user_name = data.loc[3, "Unnamed: 5"]
    full_address = set_full_address(
        data.loc[5, "Unnamed: 5"],
        data.loc[6, "Unnamed: 5"],
    )
    user_branch = data.loc[7, "Unnamed: 5"]
    customer_id = data.loc[9, "Unnamed: 20"]
    account_name_en = data.loc[11, "Unnamed: 7"]
    account_number = data.loc[11, "Unnamed: 20"]
    currency = data.loc[13, "Unnamed: 7"]
    iban = data.loc[13, "Unnamed: 20"]
    month = data.loc[9, "Unnamed: 7"]
    return {
        "user_name": user_name,
        "user_address": full_address,
        "user_branch": user_branch,
        "customer_id": customer_id,
        "account_name_en": account_name_en,
        "account_number": account_number,
        "currency": currency,
        "iban": iban,
        "month": month,
    }


# pprint(extract_account_details(account_detail_info))


def get_report_month(data):
    return data.loc[9, "Unnamed: 7"]


# def extract_account_transactions(data):
#     # Remove columns with all NaN values
#     data = data.dropna(axis=1, how="all")
#     rows = []
#     prev_date = None

#     for _, row in data.iterrows():
#         row_dict = {}
#         row_values = row.values.tolist()

#         # Check if the date column is empty
#         if pd.isnull(row_values[0]):
#             row_values[0] = prev_date
#         else:
#             prev_date = row_values[0]

#         # Replace "nan" values with None
#         row_values = [value if value == value else None for value in row_values]

#         # Exclude rows with all NaN values
#         if not all(value is None for value in row_values[1:]):
#             for i, value in enumerate(row_values):
#                 row_dict[i] = value
#             rows.append(row_dict)
#     return rows


# extract_account_transactions(transaction_data)
# pprint(extract_account_transactions(transaction_data))


def extract_account_transactions(file_path):
    wb = xlrd.open_workbook(file_path, logfile=open(os.devnull, "w"))
    data = pd.read_excel(
        wb,
        usecols=[
            "Unnamed: 2",
            "Unnamed: 3",
            "Unnamed: 4",
            "Unnamed: 5",
            "Unnamed: 6",
            "Unnamed: 7",
            "Unnamed: 8",
            "Unnamed: 9",
            "Unnamed: 10",
            "Unnamed: 11",
            "Unnamed: 12",
            "Unnamed: 13",
            "Unnamed: 14",
            "Unnamed: 15",
            "Unnamed: 16",
            "Unnamed: 17",
            "Unnamed: 18",
            "Unnamed: 19",
            "Unnamed: 20",
            "Unnamed: 21",
            "Unnamed: 22",
            "Unnamed: 23",
            "Unnamed: 24",
            "Unnamed: 25",
        ],
    )
    data = data[18:-13].reset_index(drop=True)

    result = []
    current_posting_date = None
    current_particulars = []
    current_debit = None
    current_credit = None
    current_balance = None
    current_value_date = None

    for i in range(1, len(data) - 1):
        row = data.iloc[i]
        posting_date = row["Unnamed: 2"]
        particulars = row["Unnamed: 9"]
        debit = row["Unnamed: 18"]
        credit = row["Unnamed: 23"]
        balance = row["Unnamed: 25"]
        value_date = row["Unnamed: 5"]

        if pd.notnull(posting_date):
            if current_posting_date is not None:
                result.append(
                    {
                        "posting_date": current_posting_date,
                        "value_date": current_value_date,
                        "particulars": ", ".join(current_particulars),
                        "debit": current_debit,
                        "credit": current_credit,
                        "balance": current_balance,
                    }
                )
                current_particulars = []

            current_posting_date = posting_date
            current_value_date = value_date
            current_particulars.append(particulars) if pd.notnull(particulars) else None
            current_debit = debit
            current_credit = credit
            current_balance = balance

        elif pd.notnull(particulars):
            current_particulars.append(particulars)

    # append the last set of values
    if current_posting_date is not None:
        result.append(
            {
                "posting_date": current_posting_date,
                "value_date": current_value_date,
                "particulars": ", ".join(current_particulars),
                "debit": current_debit,
                "credit": current_credit,
                "balance": current_balance,
            }
        )

    return result


def get_first_row_value(data):
    first_row = data.iloc[0]
    return {
        "posting_date": first_row["Unnamed: 2"],
        "value_date": first_row["Unnamed: 5"],
        "particulars": first_row["Unnamed: 9"],
        "debit": first_row["Unnamed: 18"],
        "credit": first_row["Unnamed: 23"],
        "balance": first_row["Unnamed: 25"],
    }


def get_last_row_value(data):
    last_row = data.iloc[-1]
    return {
        "posting_date": last_row["Unnamed: 2"],
        "value_date": last_row["Unnamed: 5"],
        "particulars": last_row["Unnamed: 9"],
        "debit": last_row["Unnamed: 18"],
        "credit": last_row["Unnamed: 23"],
        "balance": last_row["Unnamed: 25"],
    }


# pprint(extract_account_transactions(transaction_data))
# pprint(get_first_row_value(transaction_data))
# pprint(get_last_row_value(transaction_data))
