# import time

# from selenium import webdriver
# from selenium.webdriver.chrome.options import Options
# from selenium.webdriver.common.by import By
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.support.ui import Select, WebDriverWait

# chrome_options = webdriver.ChromeOptions()
# prefs = {"profile.managed_default_content_settings.images": 2}
# chrome_options.add_experimental_option("prefs", prefs)

# profile_setting_url = "https://ebanking.cibeg.com/CIBInternet240/#/root/profileSettings"
# username = ""
# password = ""

# # browser = webdriver.Chrome(chrome_options=chrome_options)
# browser = webdriver.Chrome()
# url = "https://netbanking.mashreqbank.com/B005/ENULogin.jsp"
# browser.get(url)

# time.sleep(2)

# username_el = browser.find_element_by_name("fldLoginUserLocal")
# password_el = browser.find_element_by_name("fldLocalPassword")
# submit_btn_el = browser.find_element_by_css_selector("input[value=SignOn]")

# time.sleep(1)

# username_el.send_keys(username)
# password_el.send_keys(password)

# time.sleep(1)
# submit_btn_el.click()

# popup_window = browser.window_handles[1]  # *
# browser.switch_to.window(popup_window)  # *

# """
#     for checking whether the given frame is available to
#     switch to.  If the frame is available it switches the given driver to the
#     specified frame.
# """

# WebDriverWait(browser, 10).until(
#     EC.frame_to_be_available_and_switch_to_it((By.NAME, "frame_txn"))
# )


# account_detail_el = browser.find_element_by_css_selector(
#     "#tabmenuADT"
# ).find_element_by_css_selector("a")
# account_detail_el.click()
# """
# account selection
# """
# select = Select(
#     WebDriverWait(browser, timeout=10).until(
#         EC.element_to_be_clickable((By.ID, "fldacctnodesc"))
#     )
# )
# select.select_by_index(1)
# browser.find_element_by_name("fldsubmit").click()


# def account_details():
#     data = dict()
#     table = browser.find_element_by_css_selector("#request1")
#     boxes = table.find_elements_by_css_selector("#box")
#     for box in boxes:
#         rows = box.find_elements_by_css_selector("td")
#         for i in range(0, len(rows), 2):
#             data[rows[i].get_attribute("innerText").replace(":", "")] = rows[
#                 i + 1
#             ].get_attribute("innerText")
#     return data


# # test get email and phone number
# data = dict()
# browser.switch_to_default_content()
# WebDriverWait(browser, 10).until(
#     EC.frame_to_be_available_and_switch_to_it((By.NAME, "frame_menu"))
# )
# left_menu = browser.find_element_by_id("left-menu").click()
# soft_token_reg_btn = browser.find_element_by_id("menutab3").click()
# browser.switch_to_default_content()
# WebDriverWait(browser, 10).until(
#     EC.frame_to_be_available_and_switch_to_it((By.NAME, "frame_txn"))
# )
# middle_panel = WebDriverWait(browser, timeout=10).until(
#     lambda d: d.find_elements_by_css_selector(".middlepanel")[1]
# )
# try:
#     print(middle_panel.text)
# except:
#     pass

# for ele in middle_panel.find_elements_by_css_selector("tr")[:2]:
#     d = ele.get_attribute("innerText").split(":")
#     key, value = d[0], d[1]
#     data[key] = value
# browser.switch_to_default_content()
# WebDriverWait(browser, 10).until(
#     EC.frame_to_be_available_and_switch_to_it((By.NAME, "frame_txn"))
# )
# print(data)


import time
from pprint import pprint

import tabula
from selenium.webdriver.common.by import By

from .scrap import Scrap


class MASHRAQScrap(Scrap):
    url = "https://netbanking.mashreqbank.com/B005/ENULogin.jsp"
    profile_setting_url = (
        "https://ebanking.cibeg.com/CIBInternet240/#/root/profileSettings"
    )
    bank = "MASHRAQ"

    def scrap(self):
        try:
            self.browser.get(self.url)
            self.browser.implicitly_wait(10)
            user_id_el = self.browser.find_element(By.ID, "fldLoginUserLocal")
            user_id_el.send_keys(self.username)
            time.sleep(1)
            password_el = self.browser.find_element(By.ID, "fldLocalPassword")
            password_el.send_keys(self.password)
            time.sleep(1)
            submit_btn = self.browser.find_element(By.NAME, "imageField")
            submit_btn.click()
        except Exception as e:
            raise e
        self.browser.close()
        self.browser.quit()
