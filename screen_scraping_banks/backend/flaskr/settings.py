import os
from pathlib import Path

BASE_DIR = Path(__file__).resolve()
BACKEND = BASE_DIR.parent.parent.parent

files = os.listdir(BACKEND)

driver_file = next((file for file in files if file.startswith("chromedriver")), None)

if driver_file:
    DRIVER_PATH = os.path.join(BACKEND, driver_file)
    print("DRIVER PATH:", DRIVER_PATH)
else:
    DRIVER_PATH = BACKEND
    print("Chromedriver file not found!", DRIVER_PATH)

DATA_HUB_DIR = os.path.join(BACKEND, "data-hub")
