# from selenium.webdriver.support import expected_conditions as EC
import logging
import os
import shutil
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service as ChromeService

from .settings import DATA_HUB_DIR, DRIVER_PATH

logging.basicConfig(level=logging.INFO)


def set_chrome_options() -> Options:
    """Sets chrome options for Selenium.
    Chrome options for headless browser is enabled.
    """
    chrome_options = Options()
    # chrome_options.binary_location = "C:\\path\\to\\chrome.exe"
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--disable-dev-shm-usage")
    chrome_options.add_argument("--disable-gpu")
    # chrome_options.add_argument("--incognito")
    chrome_options.add_argument("--start-maximized")
    chrome_prefs = {}
    chrome_options.experimental_options["prefs"] = chrome_prefs
    chrome_prefs["profile.default_content_settings"] = {"images": 2}
    chrome_prefs["download.default_directory"] = None
    chrome_prefs["download.prompt_for_download"] = False
    chrome_prefs["download.directory_upgrade"] = True
    chrome_prefs["safebrowsing.enabled"] = True
    return chrome_options


class Scrap:
    """Base Scrap Class"""

    url = None
    profile_setting_url = None
    bank = None

    def __init__(self, username, password, type=None):
        self.username = username
        self.password = password
        self.type = type
        chrome_options = set_chrome_options()
        chrome_prefs = chrome_options.experimental_options["prefs"]

        # set the deafult download path
        if type:
            self.download_default_directory = chrome_prefs[
                "download.default_directory"
            ] = os.path.normpath(
                f"{DATA_HUB_DIR}/{self.bank}/{self.username}/{self.type}"
            )
        else:
            self.download_default_directory = chrome_prefs[
                "download.default_directory"
            ] = os.path.normpath(f"{DATA_HUB_DIR}/{self.bank}/{self.username}")
        service = ChromeService(
            executable_path=DRIVER_PATH, service_args=["--log-level=DEBUG"]
        )
        try:
            self.browser = webdriver.Chrome(options=chrome_options, service=service)
            logging.info("===>> Driver setted successfully.")
        except Exception as e:
            print("Error:", e)
        time.sleep(1)

    def get_dir_files(self, directory=None):
        if self.type:
            directory = os.path.join(DATA_HUB_DIR, self.bank, self.username, self.type)
        else:
            directory = os.path.join(DATA_HUB_DIR, self.bank, self.username)
        file_list = []

        for root, dirs, files in os.walk(directory):
            for file in files:
                file_list.append(os.path.join(root, file))
        return file_list

    def delete_files(self, directory=None):
        directory = os.path.join(DATA_HUB_DIR, self.bank)
        for root, dirs, files in os.walk(directory, topdown=False):
            for dir in dirs:
                dir_path = os.path.join(root, dir)
                shutil.rmtree(dir_path)
