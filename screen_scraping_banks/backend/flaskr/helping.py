from functools import wraps

from flask import Flask, abort, jsonify, redirect, request
from flask_cors import CORS
from jwt_api import JWT_API
from wrapers import require_auth

'''
to split 
'''
def split_balance(b):
    balance = b.replace(',',' ').replace('.',' ').split()
    balance = int(balance[1]+balance[2])
    return balance

app = Flask(__name__)
CORS(app)
cl = JWT_API()




@app.route('/headers',methods=['POST','GET'])
@require_auth
def headers(payload):
    print(payload)
    return jsonify({ 'payload': payload})

@app.route('/index',methods=['POST','GET'])
@require_auth
def index(p):
    print()
    return jsonify({ 'payload': True })

@app.route('/jwt',methods=['GET'])
def send_jwt():
    user_id = '111222333'
    data = {'user_id':user_id}
    jwt = cl.create_jwt(data)
    return jsonify({ 'jwt': jwt })




