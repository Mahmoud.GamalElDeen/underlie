import time
from pprint import pprint

import tabula
from selenium.webdriver.common.by import By

from .scrap import Scrap


class HSBCScrap(Scrap):
    url = "https://www.hsbc.com.eg/ways-to-bank/online/"
    profile_setting_url = ""
    bank = "HSBC"

    def scrap(self, answer):
        try:
            self.browser.get(self.url)
            self.browser.implicitly_wait(5)
            log_on_el = self.browser.find_element(By.ID, "pp_tools_button_3")
            log_on_el.click()
            popup_window = self.browser.window_handles[1]
            self.browser.switch_to.window(popup_window)
            time.sleep(1)

            user_name_el = self.browser.find_element(By.ID, "Username1")
            user_name_el.send_keys(self.username)
            submit_btn_1 = self.browser.find_element(
                By.XPATH,
                "/html/body/div[2]/div[3]/div/div[2]/div/form/div[2]/div[2]/span/input",
            )
            submit_btn_1.click()
            time.sleep(1)
            answer_el = self.browser.find_element(By.ID, "memorableAnswer")
            answer_el.send_keys(answer)
            time.sleep(1)

            index_0 = self.password[0]
            index_1 = self.password[1]
            index_2 = self.password[2]

            password_el_0 = self.browser.find_element(By.ID, "pass1")
            password_el_0.send_keys(index_0)

            password_el_1 = self.browser.find_element(By.ID, "pass3")
            password_el_1.send_keys(index_1)

            password_el_2 = self.browser.find_element(By.ID, "pass8")
            password_el_2.send_keys(index_2)

            submit_btn = self.browser.find_element(By.CLASS_NAME, "submit_input")
            submit_btn.click()
            time.sleep(5)
        except Exception as e:
            raise e
        self.browser.close()
        self.browser.quit()
