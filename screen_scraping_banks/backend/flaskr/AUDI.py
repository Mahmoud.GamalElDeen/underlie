import time
from pprint import pprint

import tabula
from selenium.webdriver.common.by import By

from .scrap import Scrap


class AUDIScrap(Scrap):
    url = "https://audionline.bankaudi.com.lb/CustomLogin/RetailLogin.aspx?sID=-1"
    profile_setting_url = ""
    bank = "AUDI"

    def scrap(self):
        try:
            self.browser.get(self.url)
            self.browser.implicitly_wait(5)

            user_name_el = self.browser.find_element(By.ID, "txtName")
            user_name_el.send_keys(self.username)

            password_el = self.browser.find_element(By.ID, "txtStep1Pass_fake")
            password_el.send_keys(self.password)

            submit_btn = self.browser.find_element(
                By.ID,
                "SubmitStep1",
            )
            submit_btn.click()
            time.sleep(5)
        except Exception as e:
            raise e
        self.browser.close()
        self.browser.quit()
