import time
from pprint import pprint

import tabula
from selenium.webdriver.common.by import By

from .scrap import Scrap


class AIBScrap(Scrap):
    url = "https://ebanking.aib.com.eg/eai/lrr/html?TAM_OP=login&USERNAME=unauthenticated&ERROR_CODE=0x00000000&ERROR_TEXT=HPDBA0521I%20%20%20Successful%20completion&METHOD=GET&URL=%2F&REFERER=https%3A%2F%2Faib.com.eg%2F&HOSTNAME=ebanking.aib.com.eg&AUTHNLEVEL=&FAILREASON=&PROTOCOL=https&OLDSESSION=&EXPIRE_SECS=0"
    profile_setting_url = ""
    bank = "AIB"

    def scrap(self):
        try:
            self.browser.get(self.url)
            self.browser.implicitly_wait(5)

            user_name_el = self.browser.find_element(By.ID, "usernameID")
            user_name_el.send_keys(self.username)

            password_el = self.browser.find_element(By.ID, "password")
            password_el.send_keys(self.password)

            submit_btn = self.browser.find_element(
                By.XPATH,
                "/html/body/form/div/div/div[1]/div[3]/input",
            )
            submit_btn.click()
            time.sleep(5)
        except Exception as e:
            raise e
        self.browser.close()
        self.browser.quit()
