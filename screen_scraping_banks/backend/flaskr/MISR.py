import time
from pprint import pprint

import tabula
from selenium.webdriver.common.by import By

from .scrap import Scrap


class MISRScrap(Scrap):
    url = "https://bmonline.banquemisr.com/corp/AuthenticationController?FORMSGROUP_ID__=AuthenticationFG&__START_TRAN_FLAG__=Y&__FG_BUTTONS__=LOAD&ACTION.LOAD=Y&AuthenticationFG.LOGIN_FLAG=1&BANK_ID=01"
    profile_setting_url = ""
    bank = "MISR"

    def scrap(self):
        try:
            self.browser.get(self.url)
            self.browser.implicitly_wait(10)
            user_id_el = self.browser.find_element(
                By.ID, "AuthenticationFG.USER_PRINCIPAL"
            )
            user_id_el.send_keys(self.username)
            time.sleep(1)
            password_el = self.browser.find_element(
                By.ID, "AuthenticationFG.ACCESS_CODE"
            )
            password_el.send_keys(self.password)
            time.sleep(1)
            submit_btn = self.browser.find_element(By.ID, "VALIDATE_CREDENTIALS")
            submit_btn.click()
        except Exception as e:
            raise e
        self.browser.close()
        self.browser.quit()


def extract_account_transactions(file_path):
    data = tabula.read_pdf(file_path, pages="all")
    data = data[1:]
    parsed_data = []

    for d in data:
        print(d.columns)
        # d = d.dropna(axis=1, how="all")  # Remove any completely empty columns
        # d = d.dropna(axis=0, how="all")  # Remove any completely empty rows
        for _, row in d.iterrows():
            row_data = {
                "posting_date": row[3],
                "value_date": row[4],
                "particulars": row[5],
                "debit": row[1],
                "credit": row[0],
                # "balance": row["balance"],
            }
            parsed_data.append(row_data)

    return parsed_data


def get_first_row(file_path):
    data = tabula.read_pdf(file_path, pages="all")
    data = data[1:]
    parsed_data = []
    for row in data:
        row_data = {
            "posting_date": row.columns[3],
            "value_date": row.columns[4],
            "particulars": row.columns[5],
            "debit": row.columns[1],
            "credit": row.columns[0],
        }
        parsed_data.append(row_data)
    return parsed_data


# pprint(df)
# pprint(extract_account_transactions(df))
# pprint(get_first_row(df))
