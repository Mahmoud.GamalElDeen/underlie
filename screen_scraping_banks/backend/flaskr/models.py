from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship

db = SQLAlchemy()


class User(db.Model):
    """User Class for base informations"""

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)

    def __init__(self, username, password):
        self.username = username
        self.password = password

    @classmethod
    def get_or_create(cls, username, password):
        instance = cls.query.filter_by(username=username).first()
        if instance:
            return instance

        instance = cls(username=username, password=password)
        db.session.add(instance)
        db.session.commit()
        return instance


class BankDetail(db.Model):
    """User Detail Class"""

    id = db.Column(db.Integer, primary_key=True)
    user_address = db.Column(db.String(200), nullable=True)
    user_branch = db.Column(db.String(100), nullable=True)
    customer_id = db.Column(db.String(50), nullable=True)
    account_number = db.Column(db.String(50), nullable=True)
    currency = db.Column(db.String(50), nullable=True)
    iban = db.Column(db.String(50), nullable=True)
    month = db.Column(db.String(16), nullable=True)

    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    user = relationship("User", backref="details")

    def __init__(
        self,
        user_address,
        user_branch,
        customer_id,
        account_number,
        currency,
        iban,
        month,
        user,
    ):
        self.user_address = user_address
        self.user_branch = user_branch
        self.customer_id = customer_id
        self.account_number = account_number
        self.currency = currency
        self.iban = iban
        self.month = month
        self.user = user

    @classmethod
    def create(
        cls,
        user,
        user_address=None,
        user_branch=None,
        customer_id=None,
        account_number=None,
        currency=None,
        iban=None,
        month=None,
    ):
        instance = cls(
            user=user,
            user_address=user_address,
            user_branch=user_branch,
            customer_id=customer_id,
            account_number=account_number,
            currency=currency,
            iban=iban,
            month=month,
        )
        db.session.add(instance)
        db.session.commit()
        return instance

    # @classmethod
    # def get_or_create(cls, id, defaults=None, **kwargs):
    #     instance = cls.query.filter_by(id=id).first()
    #     if instance:
    #         return instance, False
    #     else:
    #         params = dict(kwargs)
    #         params.update(defaults or {})
    #         instance = cls(**params)
    #         db.session.add(instance)
    #         db.session.commit()
    #         return instance, True


class BankTransaction(db.Model):
    """Bank Transaction related to the User Class"""

    id = db.Column(db.Integer, primary_key=True)
    posting_date = db.Column(db.String(16), nullable=True)
    value_date = db.Column(db.String(16), nullable=True)
    particulars = db.Column(db.String(200), nullable=True)
    debit = db.Column(db.String(25), nullable=True)
    credit = db.Column(db.String(25), nullable=True)
    balance = db.Column(db.String(25), nullable=True)

    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    user = relationship("User", backref="transactions")

    def __init__(
        self, posting_date, value_date, particulars, debit, credit, balance, user
    ):
        self.posting_date = posting_date
        self.value_date = value_date
        self.particulars = particulars
        self.debit = debit
        self.credit = credit
        self.balance = balance
        self.user = user

    @classmethod
    def create(
        cls,
        user,
        posting_date=None,
        value_date=None,
        particulars=None,
        debit=None,
        credit=None,
        balance=None,
        commit=True,
    ):
        transaction = cls(
            posting_date=posting_date,
            value_date=value_date,
            particulars=particulars,
            debit=debit,
            credit=credit,
            balance=balance,
            user=user,
        )
        db.session.add(transaction)

        if commit:
            db.session.commit()
        return transaction

    def insert(self):
        db.session.add(self)
        db.session.commit()

    def update(self):
        db.session.commit()


# class UserData(db.Model):
#     """
#     Data Base ORM
#     """

#     __tablename__ = "questions"

#     user_id = Column(String, primary_key=True)  # user_id
#     enc_username = Column(String)  # encrypted username
#     enc_password = Column(String)  # encrypted password
#     account_num = Column(String)
#     balance = Column(String)
#     phone_num = Column(String, nullable=True)
#     address = Column(String, nullable=True)
#     email = Column(String, nullable=True)
#     branch = Column(String, nullable=True)
#     customer_id = Column(String, nullable=True)
#     iban = Column(String, nullable=True)
#     account_number = Column(String, nullable=True)

#     def __init__(
#         self,
#         user_id,
#         enc_username,
#         enc_password,
#         account_num,
#         balance,
#         phone_num,
#         address,
#         email,
#     ):
#         self.user_id = user_id
#         self.enc_username = enc_username
#         self.enc_password = enc_password
#         self.account_num = account_num
#         self.balance = balance
#         self.phone_num = phone_num
#         self.address = address
#         self.email = email

#     def insert(self):
#         db.session.add(self)
#         db.session.commit()

#     def update(self):
#         db.session.commit()

#     def delete(self):
#         db.session.delete(self)
#         db.session.commit()

#     def close(self):
#         db.session.close()

#     def format(self):
#         return {
#             "user_id": self.user_id,
#             "enc_username": self.enc_username,
#             "enc_password": self.enc_password,
#             "account_num": self.account_num,
#             "balance": self.balance,
#             "phone_num": self.phone_num,
#             "address": self.address,
#             "email": self.email,
#         }
