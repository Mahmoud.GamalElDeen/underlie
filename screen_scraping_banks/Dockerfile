# Run the dockerfile commands: 
# docker-compose -f docker-compose-dev.yml up -d
# docker build .
# docker-compose -f docker-compose-dev.yml down

# Use the official Python base image
FROM python:3.10

# Set the working directory in the container
WORKDIR /app/backend

# copy project
COPY . .

# Copy the requirements file to the container
COPY requirements.txt .

# Install Python dependencies
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN pip install Flask gunicorn

# Install system dependencies
RUN apt-get update \
    && apt-get install -y wget unzip gcc
RUN apt-get install -y curl gnupg2 unzip gcc libffi-dev

# install google chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
RUN apt-get -y update
RUN apt-get install -y google-chrome-stable --fix-missing

RUN rm -rf /var/lib/apt/lists/*

# Install Selenium and Chrome WebDriver
RUN apt-cache search unzip

RUN apt-get install -yqq unzip

RUN LATEST_CHROME_DRIVER_VERSION=$(wget -qO- https://chromedriver.storage.googleapis.com/LATEST_RELEASE) \
    && wget -O /tmp/chromedriver.zip "https://chromedriver.storage.googleapis.com/$LATEST_CHROME_DRIVER_VERSION/chromedriver_linux64.zip" \
    && unzip /tmp/chromedriver.zip -d /usr/local/bin \
    && rm /tmp/chromedriver.zip

# # Set the path to Chrome WebDriver
ENV PATH="/usr/local/bin:${PATH}"

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Install OpenJDK 17
RUN apt-get update && apt-get install -y openjdk-17-jdk

# Set debian frontend to seamless mode
ENV DEBIAN_FRONTEND=noninteractive

# Create a virtual environment
# RUN apt install python3.8-venv

# RUN python3 -m venv venv
# ENV SHELL=bash

# Set Tesseract data path
ENV TESSDATA_PREFIX=/usr/share/tesseract-ocr/5/tessdata

# Install Tesseract
RUN apt-get update && apt-get install -y tesseract-ocr

# Expose port
EXPOSE 5000

# set the path to flask Web app
ENV FLASK_APP=/app/backend/flaskr/app.py
ENV FLASK_RUN_PORT=5000

CMD ["python3", "-m" , "flask", "run", "--host=0.0.0.0"]
# Deactivate the virtual environment
# RUN . $VIRTUAL_ENV/bin/deactivate